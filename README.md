# README #

ROB 550
Bot Lab

### To get rid of clock skew on Maebot ###
find . -exec touch {} \;

make clean

make -j

### kill procman ###

killall bot-procman-deputy

killall bot-procman-sheriff

### simple skeleton of the odometry
About the new odometry

original state: state->old_state[3] //start from {0,0,0}
middle state:   delta[3]	    // which is the vector from old_state to the new xyt state
final state: state->xyt[3]	    //new pose obtained by head to tail transformation from world coordinates to the old_state and then to the new state xyt through delta vector.

algorithm:
initial old_state with prev_tick_count in the main
{
   while loop
      {
	read the current tick count and calculate the delta and sigma_dia.
	obtain the new state xyt and j_plus through function xyt_head2tail by using delta and old state
	calculate diagonal sigma by using assumed alpha and beta.
	
	if(use_gyro is false)	    
	    {
	    calculate A*sigma_dia*A', where A is 3x3
	    } 
	else
	    {
	    claculate A*sigma_dia_option*A', where A is 3x4
	    }
	calculate big_sigma by current state->Sigma and the matrix calculated by A
	calculate j_plus*big_sigma*j_plus'=sigma_delta
	publish sigma_delta
	give the current ticks and states to prev_ticks and old_state
      }
}