#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <unistd.h>

#include "math/gsl_util_vector.h"
#include "math/gsl_util_matrix.h"
#include "math/gsl_util_blas.h"

#include "xyt.h"

#define PI  3.14159265359

int main(){
	// Initialize the variables to be used
	double T[3*3];
	double j_minus[3*3];
	double j_plus[3*6];
	double j_tail[3*6];
	double x_ij[3];
	double x_ji[3];
	double x_ik[3];
	double x_jk[3];
	char test = 'z';

	// start the test loop
	while(1){

		// Ask for the function to be tested
		if(test != '\n')
			printf("Enter:\n\t'r' for xyt_rbt(),\n\t'i' for xyt_inverse(),\n\t'h' for xyt_head2tail(), \n\t't' for xyt_tail2tail(),\n\t'q' to quit.\n");
		test = 'z';
		scanf("%c",&test);

		// test xyt_rbt()
		if(test == 'r'){
			// Get the input parameters
		    printf("Enter new x: ");
		    scanf("%lf",&x_ij[0]);
		    printf("Enter new y: ");
		    scanf("%lf",&x_ij[1]);
		    printf("Enter new theta: ");
		    scanf("%lf",&x_ij[2]);
			// call the function
		    xyt_rbt(T,x_ij);
			// print the results
			printf("\n");
			printf("Input X_ij: [%f, %f, %f]\n",x_ij[0],x_ij[1],x_ij[2]);
			printf("Output T:\n");
		    for(int i = 0; i < 3; i++){
		        printf("%f\t%f\t%f\n",T[i*3],T[i*3+1],T[i*3+2]);
		    }
			printf("\n");
		}
		// test xyt_inverse()
		else if(test == 'i'){
			// Get the input parameters
		    printf("Enter new x: ");
		    scanf("%lf",&x_ij[0]);
		    printf("Enter new y: ");
		    scanf("%lf",&x_ij[1]);
		    printf("Enter new theta: ");
		    scanf("%lf",&x_ij[2]);
			// call the function
		    xyt_inverse(x_ji,j_minus,x_ij);
			// print the results
			printf("\n");
			printf("Input X_ij: [%f, %f, %f]\n",x_ij[0],x_ij[1],x_ij[2]);
			printf("Output X_ji: [%f, %f, %f]\n",x_ji[0],x_ji[1],x_ji[2]);
			printf("Output J_minus:\n");
		    for(int i = 0; i < 3; i++){
		        printf("%f\t%f\t%f\n",j_minus[i*3],j_minus[i*3+1],j_minus[i*3+2]);
		    }
			printf("\n");
		}
		// test xyt_head2tail()
		else if(test == 'h'){
			// Get the input parameters
		    printf("Enter new x: ");
		    scanf("%lf",&x_ij[0]);
		    printf("Enter new y: ");
		    scanf("%lf",&x_ij[1]);
		    printf("Enter new theta: ");
		    scanf("%lf",&x_ij[2]);
		    printf("Enter new x2: ");
		    scanf("%lf",&x_jk[0]);
		    printf("Enter new y2: ");
		    scanf("%lf",&x_jk[1]);
		    printf("Enter new theta2: ");
		    scanf("%lf",&x_jk[2]);
			// call the function
		    xyt_head2tail(x_ik,j_plus,x_ij,x_jk);
			// print the results
			printf("\n");
			printf("Input X_ij: [%f, %f, %f]\n",x_ij[0],x_ij[1],x_ij[2]);
			printf("Input X_jk: [%f, %f, %f]\n",x_jk[0],x_jk[1],x_jk[2]);
			printf("Output X_ik: [%f, %f, %f]\n",x_ik[0],x_ik[1],x_ik[2]);
			printf("Output J_plus:\n");
		    for(int i = 0; i < 3; i++){
		        printf("%f\t%f\t%f\t%f\t%f\t%f\n",j_plus[i*3],j_plus[i*3+1],j_plus[i*3+2],j_plus[i*3+3],j_plus[i*3+4],j_plus[i*3+5]);
		    }
			printf("\n");
		}
		// test xyt_tail2tail()
		else if(test == 't'){
			// Get the input parameters
		    printf("Enter new x: ");
		    scanf("%lf",&x_ij[0]);
		    printf("Enter new y: ");
		    scanf("%lf",&x_ij[1]);
		    printf("Enter new theta: ");
		    scanf("%lf",&x_ij[2]);
		    printf("Enter new x2: ");
		    scanf("%lf",&x_ik[0]);
		    printf("Enter new y2: ");
		    scanf("%lf",&x_ik[1]);
		    printf("Enter new theta2: ");
		    scanf("%lf",&x_ik[2]);
			// call the function
		    xyt_tail2tail(x_jk,j_tail,x_ij,x_ik);
			// print the results
			printf("\n");
			printf("Input X_ij: [%f, %f, %f]\n",x_ij[0],x_ij[1],x_ij[2]);
			printf("Input X_ik: [%f, %f, %f]\n",x_ik[0],x_ik[1],x_ik[2]);
			printf("Output X_jk: [%f, %f, %f]\n",x_jk[0],x_jk[1],x_jk[2]);
			printf("Output J_tail:\n");
		    for(int i = 0; i < 3; i++){
		        printf("%f\t%f\t%f\t%f\t%f\t%f\n",j_tail[i*3],j_tail[i*3+1],j_tail[i*3+2],j_tail[i*3+3],j_tail[i*3+4],j_tail[i*3+5]);
		    }
			printf("\n");
		}
		else if(test == 'q'){
			break;
		}
		sleep(1);
	}
	return 0;
}
