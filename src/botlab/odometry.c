#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include <time.h>

#include "common/getopt.h"
#include "math/gsl_util_matrix.h"
#include "math/gsl_util_blas.h"
#include "math/math_util.h"

#include "lcmtypes/maebot_motor_feedback_t.h"
#include "lcmtypes/maebot_sensor_data_t.h"
#include "lcmtypes/pose_xyt_t.h"

#include "xyt.h"

#define ALPHA_STRING          "0.0111"//"13.73"  // longitudinal covariance scaling factor
#define BETA_STRING           "0.0102"  // lateral side-slip covariance scaling factor
#define GYRO_RMS_STRING       "1.0"    // [deg/s]
#define BASE_LINE             0.08 // m distance between wheel centers
#define TICK_PERM	     4774

typedef struct state state_t;
struct state {
    getopt_t *gopt;

    lcm_t *lcm;


    const char *odometry_channel;
    const char *feedback_channel;
    const char *sensor_channel;

    // odometry params
    double meters_per_tick; // conversion factor that translates encoder pulses into linear wheel displacement
    double alpha;
    double beta;
    double gyro_rms;

    int32_t prev_left_ticks;
    int32_t prev_right_ticks;
    bool initial;
    bool use_gyro;
    bool gyro_measure;
    int64_t dtheta_utime;
    double dtheta;
    double dtheta_sigma;

    double xyt[3]; // 3-dof pose
    double Sigma[3*3];
    double old_state[3];
    int16_t bias_v_angle;
    double yaw_angle, yaw_angle_old;

    bool gyro_calibrated;
    int gyro_cnt; 
    int64_t gyro_cal[100];
    int64_t gyro_bias;
    int64_t gyro_first;
	bool first_gyro;
	FILE *fp, *gyro;
	
   // double rc;
};

/*
* Returns random double with mean 0 and standard deviation 1
*/
double
random_generator (){
	// Generates uniformly distributed rand_var [0,1]
	double rand_var1 = ((double)rand())/RAND_MAX;
	double rand_var2 = ((double)rand())/RAND_MAX;
	double rand_var = sqrt(-2*log(rand_var1))*cos(2*PI*rand_var2);
	return rand_var;
}

static void
motor_feedback_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                        const maebot_motor_feedback_t *msg, void *user)
{
    state_t *state = user;

    // get encoder ticks from lcm
    int32_t cur_left_ticks, cur_right_ticks;
    if(state->initial)
    {
	state->prev_left_ticks = msg->encoder_left_ticks;
	state->prev_right_ticks = msg->encoder_right_ticks;
	state->initial=0;

	}
    cur_left_ticks = msg->encoder_left_ticks;
    cur_right_ticks = msg->encoder_right_ticks;


    // get diff ticks
    double diff_left = (double)(cur_left_ticks - state->prev_left_ticks) * state->meters_per_tick;
    double diff_right = (double)(cur_right_ticks - state->prev_right_ticks) * state->meters_per_tick;

	// add slippage uncertainty
	double diff_l_p = diff_left;// + state->alpha*fabs(diff_left)*random_generator();
	double diff_r_p = diff_right;// + state->alpha*fabs(diff_right)*random_generator();
	double diff_s_p = 0;// + state->beta*fabs(diff_left+diff_right)*random_generator();

    
    double sigma_L=state->alpha*diff_l_p;
    double sigma_R=state->alpha*diff_r_p;
    double sigma_S=state->beta*(diff_l_p+diff_r_p);
    double sigma_theta=0.00002;
    double sigma_dia[9]={sigma_L, 0.0,     0.0,
		      0.0,     sigma_R, 0.0,
 		      0.0,     0.0,	sigma_S};	
    double sigma_dia_option[16]={sigma_L,	0.0,	0.0,	0.0,
			        0.0,		sigma_R,0.0,	0.0,
			        0.0,		0.0,	sigma_S,0.0,
			        0.0,		0.0,	0.0,	sigma_theta};

        // Odometry model
 
	double A[3*3] = {  0.5,				0.5, 		        0.0,
			   0.0,   			0.0, 			1.0,
			  -1.0/BASE_LINE,               1.0/BASE_LINE, 	        0.0};
	double A_option[3*4]={0.5,	0.5,	0.0,	0.0,
			      0.0,	0.0,	1.0,	0.0,
			      0.0,	0.0,	0.0,	1.0};
	double delta[3];
	if(state->use_gyro)
	{

			delta[0]=(diff_l_p + diff_r_p)/2;
			delta[1]=(diff_s_p);
			delta[2]=state->yaw_angle - state->yaw_angle_old;
	}
	else
	{
			delta[0]=(diff_l_p + diff_r_p)/2;
			delta[1]=(diff_s_p);
			delta[2]=(diff_r_p - diff_l_p)/BASE_LINE;
		
	}
	// Calculate Covariance
	double j_plus[3*6],x_ik[3];
	//	double x_temp[3] = {0,0,0};
	//	xyt_head2tail(x_ik,j_plus,x_temp,state->xyt);
	xyt_head2tail(x_ik,j_plus,state->old_state,delta);
	state->xyt[0] = x_ik[0];
	state->xyt[1] = x_ik[1];
	if(!state->use_gyro)
	state->xyt[2] = x_ik[2];
	else
	state->xyt[2]=state->yaw_angle;
	/*	printf("j+:\n%f\t%f\t%f\t%f\t%f\t%f\n%f\t%f\t%f\t%f\t%f\t%f\n%f\t%f\t%f\t%f\t%f\t%f\n",j_plus[0],j_plus[1],j_plus[2],j_plus[3],
		j_plus[4],j_plus[5],j_plus[6],j_plus[7],j_plus[8],j_plus[9],j_plus[10],j_plus[11],j_plus[12],
		j_plus[13],j_plus[14],j_plus[15],j_plus[16],j_plus[17]);*/

	// Calculate sigma_delta
	double sigma_delta[3*3];
	for(int i = 0; i < 9; i++){
		sigma_delta[i] = 0.0;
	}
		
	//==============A*sigma_delta*A'==============================================
	gsl_matrix *temp33=gsl_matrix_alloc(3,3);
	gsl_matrix *temp36=gsl_matrix_alloc(3,6);
        gsl_matrix *temp34=gsl_matrix_alloc(3,4);

	gsl_matrix *sigma_delta_gsl=gsl_matrix_alloc(3,3);
    if(!state->use_gyro)
    {
	gsl_matrix_view a=gsl_matrix_view_array(A,3,3);
	gsl_matrix_view s=gsl_matrix_view_array(sigma_dia,3,3);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
			1.0,&a.matrix, &s.matrix,
			0.0,temp33);
	gsl_blas_dgemm(CblasNoTrans, CblasTrans,
			1.0,temp33, &a.matrix,
			0.0,sigma_delta_gsl);
    }
    else
    {
	gsl_matrix_view a_option=gsl_matrix_view_array(A_option,3,4);
	gsl_matrix_view s_option=gsl_matrix_view_array(sigma_dia_option,4,4);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
			1.0,&a_option.matrix, &s_option.matrix,
			0.0,temp34);
	gsl_blas_dgemm(CblasNoTrans, CblasTrans,
			1.0,temp34, &a_option.matrix,
			0.0,sigma_delta_gsl);	
    }	
	
	for(int i=0;i<3;i++)
	{
		for(int j=0;j<3;j++)
		{
		sigma_delta[i*3+j]=gsl_matrix_get(sigma_delta_gsl,i,j);	
		}
	}
 

	//===========================form 6x6 sigmamatrix=========================
	gsl_matrix *big_sigma=gsl_matrix_calloc(6,6);
	gsl_matrix *new_sigma=gsl_matrix_alloc(3,3);
	for(int i=0; i<3; i++)
	   {
	     for(int j=0; j<3; j++)
		{
		gsl_matrix_set(big_sigma, i, j,state->Sigma[i*3+j]);
		}
	    }
	for(int i=3; i<6; i++)
	   {
	     for(int j=3; j<6; j++)
		{
		gsl_matrix_set(big_sigma, i, j,gsl_matrix_get(sigma_delta_gsl,i-3,j-3));
		}
	    }
	
	//=calculate the new sigma=========================================================
	gsl_matrix_view j_plus_gsl=gsl_matrix_view_array(j_plus,3,6);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
			1.0,&j_plus_gsl.matrix, big_sigma,
			0.0,temp36);
	gsl_blas_dgemm(CblasNoTrans, CblasTrans,
			1.0,temp36, &j_plus_gsl.matrix,
			0.0,new_sigma);	
		for(int i=0;i<3;i++)
	{
		for(int j=0;j<3;j++)
		{
		state->Sigma[i*3+j]=gsl_matrix_get(new_sigma,i,j);	
		}
	}
	
	//==================================================================================
    pose_xyt_t odo = { .utime = msg->utime };
    memcpy (odo.xyt, state->xyt, sizeof state->xyt);
    memcpy (odo.Sigma, state->Sigma, sizeof state->Sigma);
    pose_xyt_t_publish (state->lcm, state->odometry_channel, &odo);
//	printf("published\n");

	// Update state
    state->prev_left_ticks = cur_left_ticks;
    state->prev_right_ticks = cur_right_ticks;
    state->old_state[0] = state->xyt[0];
    state->old_state[1] = state->xyt[1];
    state->old_state[2] = state->xyt[2];
//	printf("end\n");
}

// Use linear least square to find the slope of biased gyro
int64_t find_gyro_bias(state_t *state){
    printf("started gyro cali\n");
    gsl_matrix *A;
    A = gsl_matrix_calloc(100,2);
    gsl_vector *b;
    b = gsl_vector_calloc(100);

    for (int i = 0;i < 100;i++){
        gsl_matrix_set(A,i,0,1);
        gsl_matrix_set(A,i,1,i);
        gsl_vector_set(b,i,state->gyro_cal[i]);
    }
    // (A_T * A)^-1 * A_T * b
    gsl_matrix *temp1 = gslu_blas_mTm_alloc(A,A);
    gsl_matrix *Inv = gslu_matrix_inv_alloc(temp1);
    gsl_matrix *temp2 = gslu_blas_mmT_alloc(Inv,A);
    
    gsl_vector *s = gslu_blas_mv_alloc(temp2,b);
    
    return (int64_t)gsl_vector_get(s,1);
}


static void
sensor_data_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                     const maebot_sensor_data_t *msg, void *user)
{
    state_t *state = user;

    if (!state->use_gyro)
        return;
	// Find Gyro bias and get yaw angle
    int64_t gyro_int;
    
    // Calibrate gyro
    if (state->gyro_calibrated == 0){
        state->gyro_cal[state->gyro_cnt] = msg->gyro_int[2];
        
        if (state->gyro_cnt == 99){
			for(int i = 0; i < 100; i++){
				fprintf(state->gyro, "%llu\t",state->gyro_cal[i]);
			}
            state->gyro_bias = find_gyro_bias(state);
            state->gyro_calibrated = 1;
            printf("gyro calibrated\tBias = %llu\n",state->gyro_bias);
        }
    }
    else {
        state->yaw_angle_old = state->yaw_angle;
        gyro_int = msg->gyro_int[2] - ((int64_t)state->gyro_cnt * state->gyro_bias);
        
        if(state->first_gyro){
            state->gyro_first = gyro_int;
            state->first_gyro = 0;
        }
        // Frequency = 1MHz, "FS_SEL=0" = 131
        state->yaw_angle = (gyro_int-state->gyro_first)/1.0e6 * 1.0/131.0 * M_PI / 180.0;
//        printf("state gyro = %lf\n",state->gyro);
        
        fprintf(state->fp,"%lf\n",state->yaw_angle);
        
    }
    state->gyro_cnt = state->gyro_cnt + 1;


}

int
main (int argc, char *argv[])
{
    // so that redirected stdout won't be insanely buffered.
    setvbuf (stdout, (char *) NULL, _IONBF, 0);

    state_t *state = calloc (1, sizeof *state);

	
    state->meters_per_tick = 1.0/4775; // IMPLEMENT ME

    state->gopt = getopt_create ();
    getopt_add_bool   (state->gopt, 'h', "help", 0, "Show help");
    getopt_add_bool   (state->gopt, 'g', "use-gyro", 0, "Use gyro for heading instead of wheel encoders");
    getopt_add_string (state->gopt, '\0', "odometry-channel", "BOTLAB_ODOMETRY", "LCM channel name");
    getopt_add_string (state->gopt, '\0', "feedback-channel", "MAEBOT_MOTOR_FEEDBACK", "LCM channel name");
    getopt_add_string (state->gopt, '\0', "sensor-channel", "MAEBOT_SENSOR_DATA", "LCM channel name");
    getopt_add_double (state->gopt, '\0', "alpha", ALPHA_STRING, "Longitudinal covariance scaling factor");
    getopt_add_double (state->gopt, '\0', "beta", BETA_STRING, "Lateral side-slip covariance scaling factor");
    getopt_add_double (state->gopt, '\0', "gyro-rms", GYRO_RMS_STRING, "Gyro RMS deg/s");

    if (!getopt_parse (state->gopt, argc, argv, 1) || getopt_get_bool (state->gopt, "help")) {
        printf ("Usage: %s [--url=CAMERAURL] [other options]\n\n", argv[0]);
        getopt_do_usage (state->gopt);
        exit (EXIT_FAILURE);
    }

    state->use_gyro = getopt_get_bool (state->gopt, "use-gyro");
    state->odometry_channel = getopt_get_string (state->gopt, "odometry-channel");
    state->feedback_channel = getopt_get_string (state->gopt, "feedback-channel");
    state->sensor_channel = getopt_get_string (state->gopt, "sensor-channel");
    state->alpha = getopt_get_double (state->gopt, "alpha");
    state->beta = getopt_get_double (state->gopt, "beta");
    state->gyro_rms = getopt_get_double (state->gopt, "gyro-rms") * DTOR;
	
	state->initial=1;
	state->gyro_measure=1;
	state->old_state[0]=0;
	state->old_state[1]=0;
	state->old_state[2]=0;
	for(int i = 0; i < 9; i++){
		if(i == 0 || i == 4 || i == 8) state->Sigma[i] = 0;
		else state->Sigma[i] = 0;
	}
	//state->prev_left_ticks = 0;
	//state->prev_right_ticks = 0;
	state->first_gyro = 1;
	state->gyro_cnt = 0;
	state->gyro_calibrated = 0;
	state->fp = fopen("gyro_6.txt","w");
	state->gyro = fopen("gyro_cali_1.txt","w");

	// Seed the random number generator
	srand(time(0));

    // initialize LCM
    state->lcm = lcm_create (NULL);
    maebot_motor_feedback_t_subscribe (state->lcm, state->feedback_channel,
                                       motor_feedback_handler, state);
    maebot_sensor_data_t_subscribe (state->lcm, state->sensor_channel,
                                    sensor_data_handler, state);

    printf ("ticks per meter: %f\n", 1.0/state->meters_per_tick);

    while (1)
        lcm_handle (state->lcm);
}


