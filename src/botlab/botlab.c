#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <math.h>
#include <lcm/lcm.h>
#include <time.h>

#include "vx/vx.h"
#include "vx/vxo_drawables.h"
#include "vx/vx_remote_display_source.h"

#include "common/getopt.h"
#include "common/timestamp.h"
#include "common/zarray.h"

#include "math/matd.h"
#include "math/math_util.h"
#include "math/gsl_util_vector.h"
#include "math/gsl_util_matrix.h"
#include "math/gsl_util_eigen.h"
#include "math/gsl_util_blas.h"
#include "math/homogenous.h"

#include "imagesource/image_util.h"
#include "imagesource/image_source.h"
#include "imagesource/image_convert.h"

#include "lcmtypes/maebot_diff_drive_t.h"
#include "lcmtypes/maebot_laser_t.h"
#include "lcmtypes/maebot_leds_t.h"
#include "lcmtypes/maebot_sensor_data_t.h"
#include "lcmtypes/maebot_motor_feedback_t.h"
#include "lcmtypes/pose_xyt_t.h"
#include "lcmtypes/rplidar_laser_t.h"


#include "xyt.h"

#define JOYSTICK_REVERSE_SPEED1 -0.25f
#define JOYSTICK_FORWARD_SPEED1  0.35f

#define JOYSTICK_REVERSE_SPEED2 -0.35f
#define JOYSTICK_FORWARD_SPEED2  0.45f

#define MAX_REVERSE_SPEED -0.35f
#define MAX_FORWARD_SPEED  0.35f

#define VXO_GRID_SIZE 0.25 // [m]

#define SLIDING_TIME_WINDOW 10000000 // 10 s

#define GOAL_RADIUS 0.10 // [m]

#define dmax(A,B) A < B ? B : A
#define dmin(A,B) A < B ? A : B

typedef struct state state_t;

struct state {
    bool running;
    getopt_t *gopt;
    char *url;
    image_source_t *isrc;
    int fidx;
    lcm_t *lcm;
    
    pose_xyt_t *pose;	
	
    pthread_t command_thread;
    maebot_diff_drive_t cmd;
    bool manual_control;

    pthread_t render_thread;
    bool   have_goal;
    double goal[3];
    
    vx_world_t *vw;
    vx_application_t app;
    vx_event_handler_t veh;
    zhash_t *layer_map; // <display, layer>

    pthread_mutex_t mutex;

    gslu_eigen *eigen;

    zarray_t *trajectory;
    zarray_t *ellipse;
    
    zarray_t *obs_red;
    zarray_t *obs_green;
    zarray_t *trajectory_comp;
    double current_pose[3];
    double prev_pose[3];
    int64_t time_pose;
};

typedef struct compensate compensate_t;
struct compensate{
int64_t lidar_time;
float delta_pose[3];
double poseatlidar[3];
};



static void
display_finished (vx_application_t *app, vx_display_t *disp)
{
    state_t *state = app->impl;

    pthread_mutex_lock (&state->mutex);
    {
        vx_layer_t *layer = NULL;
        zhash_remove (state->layer_map, &disp, NULL, &layer);
        vx_layer_destroy (layer);
    }
    pthread_mutex_unlock (&state->mutex);
}

static void
display_started (vx_application_t *app, vx_display_t *disp)
{
    state_t *state = app->impl;

    vx_layer_t *layer = vx_layer_create (state->vw);
    vx_layer_set_display (layer, disp);
    vx_layer_add_event_handler (layer, &state->veh);

    vx_layer_camera_op (layer, OP_PROJ_PERSPECTIVE);
    float eye[3]    = {  0,  0,  5};
    float lookat[3] = {  0,  0,  0 };
    float up[3]     = {  0,  1,  0 };
    vx_layer_camera_lookat (layer, eye, lookat, up, 1);

    pthread_mutex_lock (&state->mutex);
    {
        zhash_put (state->layer_map, &disp, &layer, NULL, NULL);
    }
    pthread_mutex_unlock (&state->mutex);
}

static int
touch_event (vx_event_handler_t *vh, vx_layer_t *vl, vx_camera_pos_t *pos, vx_touch_event_t *mouse)
{
    return 0;
}

static int
mouse_event (vx_event_handler_t *vh, vx_layer_t *vl, vx_camera_pos_t *pos, vx_mouse_event_t *mouse)
{
    state_t *state = vh->impl;

    // Button state
    bool m1 = mouse->button_mask & VX_BUTTON1_MASK;
    bool ctrl = mouse->modifiers & VX_CTRL_MASK;
    //bool alt = mouse->modifiers & VX_ALT_MASK;
    //bool shift = mouse->modifiers & VX_SHIFT_MASK;

    pthread_mutex_lock (&state->mutex);
    {
        if (m1 && ctrl) {
            // Ray cast to find click point
            vx_ray3_t ray;
            vx_camera_pos_compute_ray (pos, mouse->x, mouse->y, &ray);

            double ground[3];
            vx_ray3_intersect_xy (&ray, 0, ground);

            printf ("Mouse clicked at coords: [%8.3f, %8.3f]  Ground clicked at coords: [%6.3f, %6.3f]\n",
                    mouse->x, mouse->y, ground[0], ground[1]);

            state->goal[0] = ground[0];
            state->goal[1] = ground[1];
            state->have_goal = true;
        }
    }
    pthread_mutex_unlock (&state->mutex);

    return 0;
}

static int
key_event (vx_event_handler_t *vh, vx_layer_t *vl, vx_key_event_t *key)
{
    state_t *state = vh->impl;

    static float rev_speed = JOYSTICK_REVERSE_SPEED1;
    static float fwd_speed = JOYSTICK_FORWARD_SPEED1;
    static bool key_shift=0, key_up=0, key_down=0, key_left=0, key_right=0;

    switch (key->key_code) {
        case VX_KEY_SHIFT:
            key_shift = !key->released;
            break;
        case VX_KEY_UP:
            key_up = !key->released;
            break;
        case VX_KEY_DOWN:
            key_down = !key->released;
            break;
        case VX_KEY_LEFT:
            key_left = !key->released;
            break;
        case VX_KEY_RIGHT:
            key_right = !key->released;
            break;
        default:
            ;// silently ignore
    }

    pthread_mutex_lock (&state->mutex);
    {
        // default to zero
        state->cmd.motor_left_speed = state->cmd.motor_right_speed = 0.0;

        if (key_shift) { // speed boost
            fwd_speed = JOYSTICK_FORWARD_SPEED2;
            rev_speed = JOYSTICK_REVERSE_SPEED2;
        }
        else {
            fwd_speed = JOYSTICK_FORWARD_SPEED1;
            rev_speed = JOYSTICK_REVERSE_SPEED1;
        }

        if (key_up) { // forward
            state->cmd.motor_left_speed = fwd_speed;
            state->cmd.motor_right_speed = fwd_speed;
            if (key_left) {
                state->cmd.motor_left_speed -= 0.1;
                state->cmd.motor_right_speed += 0.1;
            }
            else if (key_right) {
                state->cmd.motor_left_speed += 0.1;
                state->cmd.motor_right_speed -= 0.1;
            }
        }
        else if (key_down) { // reverse
            state->cmd.motor_left_speed = rev_speed;
            state->cmd.motor_right_speed = rev_speed;
            if (key_left) {
                state->cmd.motor_left_speed += 0.1;
                state->cmd.motor_right_speed -= 0.1;
            }
            else if (key_right) {
                state->cmd.motor_left_speed -= 0.1;
                state->cmd.motor_right_speed += 0.1;
            }
        }
        else if (key_left) { // turn left
            state->cmd.motor_left_speed =  rev_speed;
            state->cmd.motor_right_speed = -rev_speed;
        }
        else if (key_right) { // turn right
            state->cmd.motor_left_speed = -rev_speed;
            state->cmd.motor_right_speed = rev_speed;
        }
    }
    pthread_mutex_unlock (&state->mutex);

    return 0;
}

static void
destroy (vx_event_handler_t *vh)
{
    // do nothing, since this event handler is statically allocated.
}

static state_t *global_state;
static void handler (int signum)
{
    switch (signum) {
        case SIGINT:
        case SIGQUIT:
            global_state->running = 0;
            break;
        default:
            break;
    }
}


// This thread continuously publishes command messages to the maebot
static void *
command_thread (void *data)
{
    state_t *state = data;

    while (state->running) {
        pthread_mutex_lock (&state->mutex);
        {
            //state->cmd.timestamp = utime_now();
            maebot_diff_drive_t_publish (state->lcm,  "MAEBOT_DIFF_DRIVE", &state->cmd);
        }
        pthread_mutex_unlock (&state->mutex);

        // send at 20 hz
        const int hz = 20;
        usleep (1000000/hz);
    }
    return NULL;
}

// This thread continously renders updates from the robot
static void *
render_thread (void *data)
{
    state_t *state = data;

    // Grid
    {
        vx_buffer_t *vb = vx_world_get_buffer (state->vw, "grid");
        vx_buffer_set_draw_order (vb, 0);
        vx_buffer_add_back (vb,
                            vxo_chain (vxo_mat_scale (VXO_GRID_SIZE),
                                       vxo_grid ()));
        vx_buffer_swap (vb);
    }

    // Axes
    {
        vx_buffer_t *vb = vx_world_get_buffer (state->vw, "axes");
        vx_buffer_set_draw_order (vb, 0);
        vx_buffer_add_back (vb,
                            vxo_chain (vxo_mat_scale3 (0.10, 0.10, 0.0),
                                       vxo_mat_translate3 (0.0, 0.0, -0.005),
                                       vxo_axes_styled (vxo_mesh_style (vx_red),
                                                        vxo_mesh_style (vx_green),
                                                        vxo_mesh_style (vx_black))));
        vx_buffer_swap (vb);
    }

    const int fps = 30;
    while (state->running) {
        pthread_mutex_lock (&state->mutex);
        {
            // Goal
            if (state->have_goal) {
                float color[4] = {0.0, 1.0, 0.0, 0.5};
                vx_buffer_t *vb = vx_world_get_buffer (state->vw, "goal");
                vx_buffer_set_draw_order (vb, -1);
                vx_buffer_add_back (vb,
                                    vxo_chain (vxo_mat_translate2 (state->goal[0], state->goal[1]),
                                               vxo_mat_scale (GOAL_RADIUS),
                                               vxo_circle (vxo_mesh_style (color))));
                vx_buffer_swap (vb);
            }

            // Robot
            {
                vx_buffer_t *vb = vx_world_get_buffer (state->vw, "robot");
                vx_buffer_set_draw_order (vb, 1);
                enum {ROBOT_TYPE_TRIANGLE, ROBOT_TYPE_DALEK};
                vx_object_t *robot = NULL;
                switch (ROBOT_TYPE_TRIANGLE) {
                    case ROBOT_TYPE_DALEK: {
                        float line[6] = {0.0, 0.0, 0.0, 0.104, 0.0, 0.0};
                        robot = vxo_chain (vxo_lines (vx_resc_copyf (line, 6),
                                                      2,
                                                      GL_LINES,
                                                      vxo_lines_style (vx_red, 3.0f)),
                                           vxo_mat_scale3 (0.104, 0.104, 0.151),
                                           vxo_mat_translate3 (0.0, 0.0, 0.5),
                                           vxo_cylinder (vxo_mesh_style (vx_blue)));
                        break;
                    }
                    case ROBOT_TYPE_TRIANGLE:{
                        float line[6] = {0.0, 0.0, 0.0, 0.104, 0.0, 0.0};
                        robot = vxo_chain (vxo_lines (vx_resc_copyf (line, 6),
                                                      2,
                                                      GL_LINES,
                                                      vxo_lines_style (vx_red, 3.0f)),
                                            vxo_mat_translate3 (0.0, 0.0, 0.0),
					                        vxo_mat_scale (0.104),
                                            vxo_mat_scale3 (1, 0.5, 1),
                                            vxo_triangle (vxo_mesh_style (vx_blue)));
                        break;
                    }
                    default:
                        robot = vxo_chain (
					   vxo_mat_translate3 (state->pose->xyt[0], state->pose->xyt[1], 0.0),
					   vxo_mat_rotate_z(state->pose->xyt[2]),
					   vxo_mat_scale (0.104),
                       vxo_mat_scale3 (1, 0.5, 1),
                       vxo_triangle (vxo_mesh_style (vx_blue)));
//                        break;
                }

                if (state->pose)
                    vx_buffer_add_back (vb,
                                        vxo_chain (vxo_mat_from_xyt (state->pose->xyt),
                                                   robot));
                else
                    vx_buffer_add_back (vb, robot);

                vx_buffer_swap (vb);
            }
        	// Robot Covariance
                // HINT: vxo_circle is what you want
	
	  {
		vx_buffer_t *vb = vx_world_get_buffer (state->vw, "robot_covariance");
//		vx_object_t *covariance = NULL;

		    /*float color[4] = {0,255,0,0.5};
		    covariance = vxo_chain(
				           //vxo_mat_translate3 (state->current_pose[0], state->current_pose[1], 0.0),		       
				           //vxo_mat_rotate_z(state->current_pose[2]),
				           //vxo_mat_scale3(sqrt(cov[0]),sqrt(cov[1]),1),
					       vxo_mat_copy_from_doubles(Ellipse),
					       vxo_mat_scale(0.104),
				           vxo_circle(vxo_mesh_style(color)));

		    vx_buffer_add_back (vb,covariance);*/
		    double cov_elli[16];
		    int elli = zarray_size(state->ellipse);
		    printf("number of elli = %d\n",elli);
		    for(int i = 0; i< elli;i++){
		        zarray_get(state->ellipse,i,cov_elli);
		        vx_buffer_add_back(vb,vxo_chain(vxo_mat_copy_from_doubles(cov_elli),
		                                        vxo_mat_scale(0.16),
		                                        vxo_circle(vxo_lines_style(vx_black,2.0f))));
		    }
		

		vx_buffer_swap (vb);    
    
	    }
            // Current Lidar Scan
            // HINT: vxo_points is what you want
		//printf("lidar scan\n");
	    //uncompensated
	  {
		vx_buffer_t *vb = vx_world_get_buffer (state->vw, "lidar-red");
		vx_object_t *reddots = NULL;
		int no_red=zarray_size(state->obs_red);
		float red_nodes[3*no_red];
		for(int i=0;i<no_red;i++)
		  {
		   float temp_points[3];
		   zarray_get(state->obs_red,i,temp_points);
		   red_nodes[i*3]=temp_points[0];
		   red_nodes[i*3+1]=temp_points[1];
		   red_nodes[i*3+2]=0.01;
		  }
		
		vx_resc_t *verts_red = vx_resc_copyf(red_nodes, no_red*3);
		reddots = vxo_chain(
							   vxo_points(verts_red,
                               no_red,
                               vxo_points_style(vx_red,3.0f)),
							   vxo_mat_scale(0.104)
						  );
		vx_buffer_add_back (vb,reddots);
		vx_buffer_swap (vb);
	    }
	   //compensated
	    {
		vx_buffer_t *vb = vx_world_get_buffer (state->vw, "lidar-green");
		vx_object_t *greendots = NULL;
		int no_green=zarray_size(state->obs_green);
		float green_nodes[3*no_green];
		for(int i=0;i<no_green;i++)
		  {
		   float temp_points[3];
		   zarray_get(state->obs_green,i,temp_points);
		   green_nodes[i*3]=temp_points[0];
		   green_nodes[i*3+1]=temp_points[1];
		   green_nodes[i*3+2]=temp_points[2];
		  }
		vx_resc_t *verts_green = vx_resc_copyf(green_nodes, no_green*3);
		greendots=vxo_chain(vxo_points(verts_green,
                               no_green,
                               vxo_points_style(vx_green,3.0f)),
							   vxo_mat_scale(0.104)
						  );
		vx_buffer_add_back (vb,greendots);
		vx_buffer_swap (vb);
//		zarray_clear(state->trajectory_comp);
	    }	
	   // robot trajectrory
	//printf("robot trajectory\n");
	    {
		vx_buffer_t *vb = vx_world_get_buffer (state->vw, "trace");

		
		int points_num2=zarray_size(state->trajectory);
		float line[3*points_num2];
		for(int i=0;i<points_num2;i++)
		{
		float pts[3];
		zarray_get(state->trajectory,i,pts);
		line[3*i]=pts[0];
		line[3*i+1]=pts[1];
		line[3*i+2]=pts[2];
		}
		vx_buffer_add_back (vb,
                    vxo_lines (vx_resc_copyf (line, 3*points_num2),
                               points_num2,
                               GL_LINE_STRIP,
                               vxo_lines_style (vx_red, 2.0f)));
		vx_buffer_swap (vb);
	    }
        }
        pthread_mutex_unlock (&state->mutex);
        usleep (1000000/fps);
    }

    return NULL;
}

// === LCM Handlers =================
static void
maebot_motor_feedback_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                               const maebot_motor_feedback_t *msg, void *user)
{
    state_t *state = user;

    pthread_mutex_lock (&state->mutex);
    {
        // IMPLEMENT ME
    }
    pthread_mutex_unlock (&state->mutex);
}

static void
maebot_sensor_data_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                            const maebot_sensor_data_t *msg, void *user)
{
    state_t *state = user;

    pthread_mutex_lock (&state->mutex);
    {
        // IMPLEMENT ME
    }
    pthread_mutex_unlock (&state->mutex);
}

static void
pose_xyt_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                  const pose_xyt_t *msg, void *user)
{
    state_t *state = user;

    pthread_mutex_lock (&state->mutex);
    {
        // IMPLEMENT ME
        if (state->pose) pose_xyt_t_destroy(state->pose);
        state->pose = pose_xyt_t_copy(msg);

		state->current_pose[0]=msg->xyt[0];
		state->current_pose[1]=msg->xyt[1];
		state->current_pose[2]=msg->xyt[2];
		state->time_pose=msg->utime;

  //printf("current pose: %f\t%f\t""%"PRId64"\n,",state->current_pose[0],state->current_pose[1],state->time_pose);
	double current_sigma[9];
		for(int i=0;i<9;i++)
		{
		current_sigma[i]=msg->Sigma[i];		
		}
	
//	gsl_matrix_view a=gsl_matrix_view_array(current_sigma,3,3);
    double dist = sqrt(pow(state->current_pose[0]-state->prev_pose[0],2) + pow(state->current_pose[1]-state->prev_pose[1],2));
    printf("dist = %lf\n",dist);
    if((dist >= 0.045) && (dist <= 0.055)){
	gsl_matrix *a = gsl_matrix_calloc(2,2);
	gsl_matrix_set(a,0,0,msg->Sigma[0]);
	gsl_matrix_set(a,0,1,msg->Sigma[1]);
	gsl_matrix_set(a,1,0,msg->Sigma[3]);
	gsl_matrix_set(a,1,1,msg->Sigma[4]);
	state->eigen=gslu_eigen_decomp_alloc(a);
	
			double cov[2];

			//gsl_vector *cov_eigen=state->eigen->D;
			//printf("vxo_covariance show\n");
			cov[0]=gsl_vector_get(state->eigen->D,0);
			cov[1]=gsl_vector_get(state->eigen->D,1);

		    // Sqrt of D
		    gsl_matrix * D_sqrt = gsl_matrix_calloc(2,2);
		    gsl_matrix_set(D_sqrt,0,0,sqrt(cov[0]));
		    gsl_matrix_set(D_sqrt,1,1,sqrt(cov[1]));
		
		    // V * D_sqrt
		    gsl_matrix *V_D_sqrt = gslu_blas_mm_alloc(state->eigen->V,D_sqrt);
		
		    // Build transformation matrix for Ellipse
		    double Ellipse[4*4] = {V_D_sqrt->data[0], V_D_sqrt->data[1], 0, state->current_pose[0],
							       V_D_sqrt->data[2], V_D_sqrt->data[3], 0, state->current_pose[1],
								    0,				   0,				 1, 	0,
								    0,				   0,				 0, 	1 };
            zarray_add(state->ellipse,Ellipse);
            	
	    state->prev_pose[0]=state->current_pose[0];
		state->prev_pose[1]=state->current_pose[1];
		state->prev_pose[2]=state->current_pose[2];
		}
	        

	float pts[3]={state->current_pose[0],state->current_pose[1],0.0};
	zarray_add(state->trajectory,pts);
	compensate_t traj;
	traj.delta_pose[0]=state->current_pose[0];
	traj.delta_pose[1]=state->current_pose[1];
	traj.delta_pose[2]=state->current_pose[2];
	traj.lidar_time=state->time_pose;
	zarray_add(state->trajectory_comp,&traj);

    }
    pthread_mutex_unlock (&state->mutex);
}

static void
rplidar_laser_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                       const rplidar_laser_t *msg, void *user)
{
    state_t *state = user;

    pthread_mutex_lock (&state->mutex);
    {
       // IMPLEMENT ME
	compensate_t greens;
	int idx=0;
//	compensate_t traj_lag;
	int32_t nranges_bot=msg->nranges;
	zarray_clear(state->obs_red);
	zarray_clear(state->obs_green);
	//zarray_clear(state->trajectory_comp);

	//printf("the number of ranges: ""%"PRId32"\n",nranges_bot);
	float *ranges_bot=msg->ranges;
	float *thetas_bot=msg->thetas;
	int64_t *times_bot=msg->times;
	for(int i=0;i<nranges_bot;i++){	
		double J_plus[3*6];
		double x_ik[3];
		double delta_range[3];

		delta_range[0]=ranges_bot[i]*cos(-thetas_bot[i]);
		delta_range[1]=ranges_bot[i]*sin(-thetas_bot[i]);
		delta_range[2]=-thetas_bot[i];
		xyt_head2tail(x_ik,J_plus,state->current_pose,delta_range);
		float red_pts[3];
		red_pts[0]=x_ik[0];
		red_pts[1]=x_ik[1];
		red_pts[2]=0.0;
		zarray_add(state->obs_red,red_pts);
/*		// compensated model	
		greens.delta_pose[0]=delta_range[0];
		greens.delta_pose[1]=delta_range[1];
		greens.delta_pose[2]=delta_range[2];
		greens.lidar_time=times_bot[i];
		zarray_add(state->obs_green,&greens);
*/	
		//printf("lidar time range is[""%"PRId64"\t""%"PRId64"]\n",times_bot[0],times_bot[nranges_bot-1]);
		int segment=zarray_size(state->trajectory_comp);
		int index = segment - 1;
//edited
		
		float green_temp[3];
		zarray_get(state->trajectory_comp,idx,&greens);
//edited
		compensate_t pose1;
		compensate_t pose2;
		double lag_pose[3];
//edited
		if(idx<segment-1)
		{
		  if(times_bot[i]<greens.lidar_time)
		   {
		   delta_range[0]=ranges_bot[i]*cos(-thetas_bot[i]);
		   delta_range[1]=ranges_bot[i]*sin(-thetas_bot[i]);
		   delta_range[2]=thetas_bot[i];
		   lag_pose[0]=greens.delta_pose[0];
		   lag_pose[1]=greens.delta_pose[1];
		   lag_pose[2]=greens.delta_pose[2];
		
		   xyt_head2tail(x_ik,J_plus,lag_pose,delta_range);
		   green_temp[0]=x_ik[0];
		   green_temp[1]=x_ik[1];
		   green_temp[2]=0.0;
		   zarray_add(state->obs_green,green_temp);
		   }
		  else{
		   idx=idx+1;
		   zarray_get(state->trajectory_comp,idx,&greens);
		   }
		}
		else
		{
		   delta_range[0]=ranges_bot[i]*cos(-thetas_bot[i]);
		   delta_range[1]=ranges_bot[i]*sin(-thetas_bot[i]);
		   delta_range[2]=thetas_bot[i];
		   lag_pose[0]=greens.delta_pose[0];
		   lag_pose[1]=greens.delta_pose[1];
		   lag_pose[2]=greens.delta_pose[2];
		
		   xyt_head2tail(x_ik,J_plus,lag_pose,delta_range);
		   green_temp[0]=x_ik[0];
		   green_temp[1]=x_ik[1];
		   green_temp[2]=0.0;
		   zarray_add(state->obs_green,green_temp);		
		}
//edited




		/*if((times_bot[i] < state->time_pose) && (segment > 0)){
			zarray_get(state->trajectory_comp, index, &pose1);
			while (times_bot[i] < pose1.lidar_time){
				index = index - 1;
				zarray_get(state->trajectory_comp, index, &pose1);
			}
			zarray_get(state->trajectory_comp, index+1, &pose2);

			int64_t time_diff = pose2.lidar_time - pose1.lidar_time;
			
			lag_pose[0] = (pose2.delta_pose[0] - pose1.delta_pose[0])/(time_diff)*(times_bot[i] - pose1.lidar_time) + pose1.delta_pose[0];
			lag_pose[1] = (pose2.delta_pose[1] - pose1.delta_pose[1])/(time_diff)*(times_bot[i] - pose1.lidar_time) + pose1.delta_pose[1];
			lag_pose[2] = (pose2.delta_pose[2] - pose1.delta_pose[2])/(time_diff)*(times_bot[i] - pose1.lidar_time) + pose1.delta_pose[2];
		}
		else{
			lag_pose[0] = state->current_pose[0];
			lag_pose[1] = state->current_pose[1];
			lag_pose[2] = state->current_pose[2];
		}*/
		// compensated model	
		double J_plus_g[3*6];
		double x_ik_g[3];

		xyt_head2tail(x_ik_g,J_plus_g,lag_pose,delta_range);
		float green_pts[3];
		green_pts[0]=x_ik[0];
		green_pts[1]=x_ik[1];
		green_pts[2]=0.0;
		zarray_add(state->obs_green,green_pts);
//edited
		zarray_clear(state->trajectory_comp);		
				
	}
		
    }
    pthread_mutex_unlock (&state->mutex);
}

state_t *
state_create (void)
{
    state_t *state = calloc (1, sizeof (*state));

    state->running = 1;
    state->gopt = getopt_create ();
    state->lcm = lcm_create (NULL);
  
    state->have_goal = false;
    state->trajectory=zarray_create(sizeof(float[3]));
    state->ellipse=zarray_create(sizeof(double[16]));
    state->obs_green=zarray_create(sizeof(float[3]));
    state->obs_red=zarray_create(sizeof(compensate_t));
    state->trajectory_comp=zarray_create(sizeof(compensate_t));
    state->prev_pose[0] = 0.;
    state->prev_pose[1] = 0.;
    state->prev_pose[2] = 0.;
    
    state->vw = vx_world_create ();
    state->app.display_finished = display_finished;
    state->app.display_started = display_started;
    state->app.impl = state;
    state->veh.dispatch_order = -10;
    state->veh.touch_event = touch_event;
    state->veh.mouse_event = mouse_event;
    state->veh.key_event = key_event;
    state->veh.destroy = destroy;
    state->veh.impl = state;
    state->layer_map = zhash_create (sizeof(vx_display_t*), sizeof(vx_layer_t*), zhash_ptr_hash, zhash_ptr_equals);

    // note, pg_sd() family of functions will trigger their own callback of my_param_changed(),
    // hence using a recursive mutex avoids deadlocking when using pg_sd() within my_param_changed()
    pthread_mutexattr_t attr;
    pthread_mutexattr_init (&attr);
    pthread_mutexattr_settype (&attr, PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init (&state->mutex, &attr);


    return state;
}

int
main (int argc, char *argv[])
{
    // so that redirected stdout won't be insanely buffered.
    setvbuf (stdout, (char *) NULL, _IONBF, 0);

    vx_global_init ();

    state_t *state = NULL;
    global_state = state = state_create ();

    // Clean up on Ctrl+C
    signal (SIGINT, handler);
    signal (SIGQUIT, handler);

    getopt_add_bool (state->gopt, 'h', "help", 0, "Show this help");
    getopt_add_int (state->gopt, 'l', "limitKBs", "-1", "Remote display bandwith limit in KBs. < 0: unlimited.");
    getopt_add_int (state->gopt, 'p', "port", "15151", "Vx display port");
    getopt_add_string (state->gopt, '\0', "maebot-motor-feedback-channel", "MAEBOT_MOTOR_FEEDBACK", "LCM channel name");
    getopt_add_string (state->gopt, '\0', "maebot-sensor-data-channel", "MAEBOT_SENSOR_DATA", "LCM channel name");
    getopt_add_string (state->gopt, '\0', "maebot-diff-drive-channel", "MAEBOT_DIFF_DRIVE", "LCM channel name");
    getopt_add_string (state->gopt, '\0', "odometry-channel", "BOTLAB_ODOMETRY", "LCM channel name");
    getopt_add_string (state->gopt, '\0', "rplidar-laser-channel", "RPLIDAR_LASER", "LCM channel name");


    if (!getopt_parse (state->gopt, argc, argv, 0)) {
        getopt_do_usage (state->gopt);
        exit (EXIT_FAILURE);
    }
    else if (getopt_get_bool (state->gopt,"help")) {
        getopt_do_usage (state->gopt);
        exit (EXIT_SUCCESS);
    }

    // Set up Vx remote display
    vx_remote_display_source_attr_t remote_attr;
    vx_remote_display_source_attr_init (&remote_attr);
    remote_attr.max_bandwidth_KBs = getopt_get_int (state->gopt, "limitKBs");
    remote_attr.advertise_name = "Maebot App";
    remote_attr.connection_port = getopt_get_int (state->gopt, "port");
    vx_remote_display_source_t *remote = vx_remote_display_source_create_attr (&state->app, &remote_attr);

    // Video stuff?

    // LCM subscriptions
    maebot_motor_feedback_t_subscribe (state->lcm,
                                       getopt_get_string (state->gopt, "maebot-motor-feedback-channel"),
                                       maebot_motor_feedback_handler, state);
    maebot_sensor_data_t_subscribe (state->lcm,
                                    getopt_get_string (state->gopt, "maebot-sensor-data-channel"),
                                    maebot_sensor_data_handler, state);
    pose_xyt_t_subscribe (state->lcm,
                          getopt_get_string (state->gopt, "odometry-channel"),
                          pose_xyt_handler, state);
    rplidar_laser_t_subscribe (state->lcm,
                               getopt_get_string (state->gopt, "rplidar-laser-channel"),
                               rplidar_laser_handler, state);

    // Launch worker threads
    pthread_create (&state->command_thread, NULL, command_thread, state);
    pthread_create (&state->render_thread, NULL, render_thread, state);

    // Loop forever
    while (state->running)
    lcm_handle_timeout (state->lcm, 500);

    pthread_join (state->command_thread, NULL);
    pthread_join (state->render_thread, NULL);

    printf ("waiting vx_remote_display_source_destroy...");
    vx_remote_display_source_destroy (remote);
    printf ("done\n");
}

